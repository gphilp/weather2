# Welcome to Simple Weather app #



### What is this repository for? ###

* Simple Weather app is just that a simple weather application built using Ionic 2 and Angular 2. The purpose of the application is to deliver the current "Feels Like" temperature and then displays in a smaller font the actual temp.
* Version 1.0

### How do I get set up? ###

You will need the following Node packages to get this up and running

* npm install -g gulp
* npm install -g ionic
* npm install -g cordova

Once you have the packages installed and weather2 downloaded

* cd to weather2
* npm install
* ionic serve[1]

[1] Currently I have the Codrops rain effect added, however, the JS is throwing a Promise error. In order to get the application working, you will need to remove the script index.min.js from  ```www/build/index.html```


### Future plans ###

* Integrate geolocation for search
* Allow users in settings to upload their favorite image for the weather page background
* Add weather effect depending on current condition. Using Codrops [Rain & Water Effects Experiment](http://tympanus.net/Development/RainEffect/)

### Known Bugs ###

* Windows 10 Node CMD line throws MSBUILD v4.0 is not supported, aborting
* Settings page: when clicking "Save Default Location" makes the Settings Page root and does not redirect to an updated Weather page
* Settings page: selecting a city from autocomplete should not write to local storage until the user clicks "Save Default Location"
* Weather page: there is no way to clear the search without restarting the application