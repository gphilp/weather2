import {Injectable, Inject} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()

export class WeatherService {

    private http: any;
    private apiKey: any;
    private conditionsUrl: any;
    private searchUrl: any;

    static get parameters() {
        return [Http];
    }

    constructor(http) {
        this.http = http;
        this.apiKey = '191291a5d256b427';
        this.conditionsUrl = 'http://api.wunderground.com/api/'+this.apiKey+'/conditions/q';
        this.searchUrl = 'http://autocomplete.wunderground.com/aq?query=';
    }

    getWeather(zmw) {
        return this.http.get(this.conditionsUrl+'/zmw:'+zmw+'.json')
            .map(res => res.json());
    }

    searchCities(searchStr) {
        return this.http.get(this.searchUrl+''+searchStr)
            .map(res => res.json());
    }
}

// http://api.wunderground.com/191291a5d256b427/conditions/q/zmw:75201.1.99999.json