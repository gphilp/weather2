import { Component, OnInit } from '@angular/core';
import { Storage, LocalStorage, NavController } from 'ionic-angular';
import {WeatherService} from '../../services/weather.service';
import {WeatherPage} from '../weather/weather';

@Component ({
    templateUrl: 'build/pages/settings/settings.html',
    providers: [WeatherService]
})

export class SettingsPage {

    private local: any;
    private weatherService: any;
    private defaultCity: any;
    private weather: any;
    private searchStr: any;
    private results: any;
    private nav: any;

    static get parameters() {
        return [[NavController], [WeatherService]];
    }

    constructor(nav, WeatherService) {
        this.local = new Storage(LocalStorage);
        this.nav = nav;
        this.weatherService = WeatherService;
        this.searchStr;
        this.defaultCity;
        this.results = [];
    }

    ngOnInit() {
        this.getDefaultCity();
    }

    getQuery() {
      this.weatherService.searchCities(this.searchStr)
          .subscribe(res => {
            this.results = res.RESULTS;
          });
    }

    chooseCity(city) {
        this.results = [];
        this.weatherService.getWeather(city.zmw)
            .subscribe(weather => {
                this.weather = weather.current_observation;
            });
    }

    getDefaultCity() {
        this.defaultCity = localStorage.getItem('name') !== null ? localStorage.getItem('name') : this.defaultCity = 'Dallas, Texas';
        console.log(this.defaultCity);
    }

    setDefaultCity(city) {
        
        this.results = [];

        if(typeof(Storage) !== "undefinded") {
            this.local.set('city', [city.zmw]);
            this.local.set('name', [city.name]);
            this.searchStr = city.name;
            this.getDefaultCity();
        } else {
            console.log('localStorage is not supported by your device or browser');
        }
    }

    saveChanges() {
        this.nav.setRoot(WeatherPage);
    }
}