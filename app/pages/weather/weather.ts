import { Component, OnInit } from '@angular/core';
import { Storage, LocalStorage } from 'ionic-angular';
import {WeatherService} from '../../services/weather.service';

interface CityResult {
  city: Object
}

@Component ({
    templateUrl: 'build/pages/weather/weather.html',
    providers: [WeatherService]
})

export class WeatherPage {

    private local: any;
    private weatherService: any;
    private weather: any;
    private searchStr: any;
    private results: any;
    private zmw: any;

    static get parameters() {
        return[[WeatherService]];
    }

    constructor(weatherService) {
      this.local = new Storage(LocalStorage);
      this.weatherService = weatherService;
      this.zmw;
      this.searchStr;
      this.weather;
      this.results;
  }

  ngOnInit() {
      this.getDefaultCity();
      this.weatherService.getWeather(this.zmw)
          .subscribe(weather => {
            this.weather = weather.current_observation;
          });
  }

  getQuery() {
      this.weatherService.searchCities(this.searchStr)
          .subscribe(res => {
            this.results = res.RESULTS;
          });
  }

  chooseCity(city) {
      this.results = [];
      this.weatherService.getWeather(city.zmw)
          .subscribe(weather => {
            this.weather = weather.current_observation;
          });
  }

  getDefaultCity() {
    this.zmw = localStorage.getItem('city') !== null ? localStorage.getItem('city') : this.zmw = '75201.1.99999';
  }
}